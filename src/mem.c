#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  // Get the actual region size
  const size_t r_size = region_actual_size(query);
  struct region r_allocated;
  // Try to allocate memory by the given address
  void* r_status = map_pages(addr, r_size, MAP_FIXED_NOREPLACE);

  if (r_status == MAP_FAILED) {
      // If we failed, try to allocate memory wherever
      r_status = map_pages(addr, r_size, 0);
      if (r_status != MAP_FAILED) {
          // Assign result to region
          r_allocated = (struct region) {
                  r_status,
                  r_size,
                  false
          };
      }
      else {
          // If we couldn't allocate memory again then return invalid region
          printf("Map failed");
          return REGION_INVALID;
        }
  }
  else {
      r_allocated = (struct region) {
              r_status,
              r_size,
              true
      };
  }
  // Do initialization with block
  block_init(r_status, (block_size) {r_size}, NULL);
  return r_allocated;

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

//#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  /* ??? */
  // Check if the block is sptlittable and it is big enough
  if (((block_splittable(block, query)) && (block_is_big_enough(query, block)))) {
        // Get the second block size
        block_size bl_size = {block->capacity.bytes - query};
        block->capacity.bytes = query;
        struct block_header* bl_next = block_after(block);
        // Make actual split (the less one is before the bigger one)
        block_init(bl_next, bl_size, block->next);
        block->next = bl_next;
        return true;
  }
  else {
    return false;
  }
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  // Do merging if we can
  if ((block->next == NULL) || (!mergeable(block, block->next))) {
      return false;
  }
  size_t new_cap;
  struct block_header* bl_next = block->next;
  new_cap = size_from_capacity(bl_next->capacity).bytes;
  
  block->capacity.bytes += new_cap;
  block->next = bl_next->next;

  return true;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  /*??? */
  struct block_header * res_block = block;

  while (true){

    if (res_block->is_free){
        // while the block is free, try to merge with next free block
        while(try_merge_with_next(res_block));

        if (block_is_big_enough(sz, res_block)){

            return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, res_block};
        }
        if (res_block->next == NULL) {
            break;
        }
        res_block = res_block->next;
    }

    else {
        if ((res_block->next) != NULL) {
            res_block = res_block->next;
        }
        break;
    }
  }
  // If the  good one is not found, return the last one
  return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, res_block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

    struct block_search_result res = find_good_or_last(block, query);
    if (res.type != BSR_FOUND_GOOD_BLOCK) {
        // Return info about the last one or bad block
        return res;
    }
    else {
      // If we found good block, try to split it if it is too big
      split_if_too_big(res.block, query);
    }
    return res;
}

void* addr_calculate(struct block_header* bl){
    // Do address calculation
    return (uint8_t*)bl + size_from_capacity(bl->capacity).bytes;
}


static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  /*  ??? */
  // Get the address which is next to the last block
  const void* addr = addr_calculate(last);
  // DO allocation if we can
  const struct region reg = alloc_region(addr, query);
  // If we couldn't make allocation correctly return NULL
  if (region_is_invalid( &reg) ) {
      return NULL;
  }
  last->next = reg.addr;
  // Try to merge the last block of the region with the new block
  if (try_merge_with_next(last)) {
      return last;
  }
  return reg.addr;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  /*  ??? */

  size_t modified_query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result res = try_memalloc_existing(modified_query, heap_start);
  // If we couldn't find the good one, do grow and split
  if (res.type == BSR_REACHED_END_NOT_FOUND){
        while (heap_start->next != NULL) {
            heap_start = heap_start->next;
        }
        res.block = grow_heap(heap_start, modified_query);
        split_if_too_big(heap_start, modified_query);
  }
  // If result is corrupted, do heap init
  if (res.type == BSR_CORRUPTED) {
        res.block = heap_init(modified_query);
        split_if_too_big(heap_start, modified_query);
  }
  res.block->is_free = false;
  return res.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

inline struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  /*  ??? */
  // When we free the block, try to merge with another free blocks
  while(try_merge_with_next(header));
}
