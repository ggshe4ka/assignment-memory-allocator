#define _DEFAULT_SOURCE
#include "tests.h"
#include "util.h"
#include "mem.h"

// Define query requests for different proposals
#define DUMMY_QUERY 1
#define OK_QUERY 300
#define BIG_QUERY 7000
#define VERY_BIG_QUERY 80000

void* init_heap_test(void * heap) {
    // Check of nullity heap
    debug_heap(stdout, heap);
    if (heap) {
        printf("[#1] Test passed \n");
    }
    else {
        printf("[#1] Test failed \n");
    }
    return heap;
}
void malloc_basic_test(void* heap) {

    // Check of the last block with query < BLOCK_MIN_CAPACITY

    void* addr_contents = _malloc(DUMMY_QUERY);
    struct block_header* header_block = block_get_header(addr_contents);
    debug_heap(stdout, heap);
    if (!addr_contents) {
        printf("[#2] Test failed with uninitialized correctly memory \n");
    }
    else {
        // Triyng to figure out if capacity is equal to min size and block is not free
        bool malloc_basic_condition = (header_block->capacity.bytes == BLOCK_MIN_CAPACITY &&
                                       header_block->is_free == false);
        if (malloc_basic_condition) {
            printf("[#2] Test passed \n");
        } else {
            printf("[#2] Test failed \n");
        }
    }

    // Clear stuff
    _free(addr_contents);
    debug_heap(stdout, heap);

}
void free_test(void* heap) {

    // Trying to get memory with normal amount of query
    void* addr_contents_1 = _malloc(OK_QUERY);
    void* addr_contents_2 = _malloc(OK_QUERY);

    debug_heap(stdout, heap);
    _free(addr_contents_1);
    debug_heap(stdout, heap);

    // Get last headers
    struct block_header* header_block_1 = block_get_header(addr_contents_1);
    struct block_header* header_block_2 = block_get_header(addr_contents_2);

    if ((!addr_contents_1) || (!addr_contents_2)){
        printf("[#3] Test failed with uninitialized correctly memory \n");
    }
    else {
        // We want the first block to be free because we've done free
        bool free_correct_condition = (header_block_1->is_free == true &&
                                       header_block_2->is_free == false);
        if (free_correct_condition) {
            printf("[#3] Test passed\n");
        } else {
            printf("[#3] Test failed\n");
        }
    }
    // Clear stuff
    _free(addr_contents_2);

}
void free_severals_test(void* heap) {
    // Use _malloc several times. E.g. we could use array and cycle for this proposal
    void* addr_contents_1 = _malloc(OK_QUERY);
    void* addr_contents_2 = _malloc(OK_QUERY);
    void* addr_contents_3 = _malloc(OK_QUERY);
    void* addr_contents_4 = _malloc(OK_QUERY);

    debug_heap(stdout, heap);
    _free(addr_contents_1);
    _free(addr_contents_2);
    _free(addr_contents_3);
    debug_heap(stdout, heap);

    struct block_header* header_block_1 = block_get_header(addr_contents_1);
    struct block_header* header_block_2 = block_get_header(addr_contents_2);
    struct block_header* header_block_3 = block_get_header(addr_contents_3);
    struct block_header* header_block_4 = block_get_header(addr_contents_4);
    if ((!addr_contents_1) || (!addr_contents_2) || (!addr_contents_3) || (!addr_contents_4)){
        printf("[#4] Test failed with uninitialized correctly memory \n");
    }
    else {
        // We want all blocks to be free except the last one
        bool free_correct_condition = (header_block_1->is_free == true &&
                                       header_block_2->is_free == true &&
                                       header_block_3->is_free == true &&
                                       header_block_4->is_free == false);
        if (free_correct_condition) {
            printf("[#4] Test passed\n");
        } else {
            printf("[#4] Test failed\n");
        }
    }
    _free(addr_contents_4);
}
void extend_test(void* heap) {

    debug_heap(stdout, heap);

    // Now we want to check out what happens if we request large amount. We want the new region to extend the last one.
    void* addr_contents_1 = _malloc(BIG_QUERY);
    void* addr_contents_2 = _malloc(BIG_QUERY);

    debug_heap(stdout, heap);

    struct block_header* header_block_1 = block_get_header(addr_contents_1);
    struct block_header* header_block_2 = block_get_header(addr_contents_2);

    if ((!addr_contents_1) || (!addr_contents_2)){
        printf("[#5] Test failed with uninitialized correctly memory \n");
    }
    else {
        bool extend_correct_condition = (header_block_1->is_free == false &&
                                         header_block_2->is_free == false);
        if (extend_correct_condition) {
            printf("[#5] Test passed\n");
        }
        else {
            printf("[#5] Test failed\n");
        }
    }
    // Clear stuff
    _free(addr_contents_2);
    _free(addr_contents_1);

}
void extend_test_2(void* heap) {

    debug_heap(stdout, heap);
    struct block_header* header_block_1 = (struct block_header*) heap;
    void* address = addr_calculate(header_block_1);
    // We want to allocate memory after the last block
    void* r_status = mmap(address,
                        40000,
                        PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
                        0,
                        0);

    void* addr_contents_1 = _malloc(VERY_BIG_QUERY);
    struct block_header* header_block_2 = block_get_header(addr_contents_1);
    debug_heap(stdout, heap);

    if (!addr_contents_1) {
        printf("[#6] Test failed with uninitialized correctly memory \n");
    }
    else {
        // Check if the new region is allocated in different place
        bool new_region_correct_condition = (r_status != MAP_FAILED &&
                                          (((long long int) addr_contents_1 - (long long int) address) > 1e6) &&
                                             header_block_2->is_free == false);
        if (new_region_correct_condition) {
            printf("[#6] Test passed\n");
        }
        else {
            printf("[#6] Test failed\n");
        }
    }
    // Clear stuff
    _free(addr_contents_1);
}
void all_unit_tests(){
    void* heap = heap_init(DUMMY_QUERY);

    printf("[!!!] Test #1: Init heap test [!!!]\n");

    heap = init_heap_test(heap);
    printf("\n");

    printf("[!!!] Test #2: Malloc basic test [!!!]\n");

    malloc_basic_test(heap);
    printf("\n");

    printf("[!!!] Test #3: Free test [!!!]\n");

    free_test(heap);
    printf("\n");

    printf("[!!!] Test #4: Free several test [!!!]\n");

    free_severals_test(heap);
    printf("\n");

    printf("[!!!] Test #5: Extend test [!!!]\n");

    extend_test(heap);
    printf("\n");

    printf("[!!!] Test #6: Extend with addit. rg. test [!!!]\n");

    extend_test_2(heap);
    printf("\n");
}